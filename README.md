# Rili Test Framework

This is module of [rili](https://gitlab.com/rilis/rili) which provide test framework similar to [gtest](https://github.com/google/googletest), but flaxible, easy to extend,
with clean code base and with much better mocks. It is used internally by rili modules in testing - even tests of rili test framework are writen in rili test framework ;)


**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/test)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

* [![build status](https://gitlab.com/rilis/rili/test/badges/master/build.svg)](https://gitlab.com/rilis/rili/test/commits/master)
* [![coverage report](https://gitlab.com/rilis/rili/test/badges/master/coverage.svg)](https://gitlab.com/rilis/rili/test/commits/master)

