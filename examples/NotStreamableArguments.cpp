#include <rili/Test.hpp>
#include "Assert.hpp"

struct SomeType {
    operator bool() const { return true; }
    bool operator==(SomeType const& other) const { return this == &other; }
    bool operator!=(SomeType const& other) const { return this != &other; }
};

TEST(NotStreamableArguments, EQ) {
    SomeType a;
    SomeType b;
    EXAMPLES_ASSERT(EXPECT_EQ(a, a));
    EXAMPLES_ASSERT(EXPECT_NE(a, b));
}

TEST(NotStreamableArguments, TRUE) {
    SomeType a;
    EXAMPLES_ASSERT(EXPECT_TRUE(a));
    EXAMPLES_ASSERT(EXPECT_FALSE(!a));
}
