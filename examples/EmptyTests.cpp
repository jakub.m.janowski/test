#include <iostream>
#include <rili/Test.hpp>
#include <rili/test/Config.hpp>
#include <stdexcept>
#include "Assert.hpp"

/*
 * simple test without fixture
 */
TEST(WithoutFixture, emptyTest) { std::cout << "WithoutFixture: emptyTest" << std::endl; }

#ifdef RILI_TEST_WITH_EXCEPTIONS
/*
 *  Tests can throw exceptions
 */
TEST(WithoutFixture, throwingTest1) {
    std::cout << "WithoutFixture: throwingTest" << std::endl;
    throw std::runtime_error("Ouch!");
}

TEST(WithoutFixture, throwingTest2) {
    std::cout << "WithoutFixture: throwingTest" << std::endl;
    throw 5;
}
#endif

/*
 * fixture which show that, first test execution sequence is ctor()->before()->test_body()->after()->dctor()
 */
class WithFixture : public rili::test::TestBaseFixture {
 public:
    WithFixture() : rili::test::TestBaseFixture() { std::cout << "WithFixture: ctor" << std::endl; }
    virtual ~WithFixture() { std::cout << "WithFixture: dctor" << std::endl; }
    void before() override { std::cout << "WithFixture: before" << std::endl; }
    void after() override { std::cout << "WithFixture: after" << std::endl; }
};

class BeforeFailFixture : public rili::test::TestBaseFixture {
 public:
    BeforeFailFixture() : rili::test::TestBaseFixture() {}
    virtual ~BeforeFailFixture() {}
    void before() override { EXPECT_EQ(1, 2); }
};

TEST_F(BeforeFailFixture, ShouldNotRunTestBody) { EXAMPLES_ASSERT(false); }

/*
 * first test which use WithFixture
 */
TEST_F(WithFixture, emptyTest1) { std::cout << "WithFixture: emptyTest1" << std::endl; }

/*
 * second test which use WithFixture. As you see fixture class can be shared, however test object instance not.
 */
TEST_F(WithFixture, emptyTest2) { std::cout << "WithFixture: emptyTest2" << std::endl; }

#ifdef RILI_TEST_WITH_EXCEPTIONS
/*
 * Bellow examples shows what will happen if fixture Ctor(), before() or after() will throw.
 */
class WithThrowingCtor : public rili::test::TestBaseFixture {
 public:
    WithThrowingCtor() : rili::test::TestBaseFixture() {
        std::cout << "WithThrowingCtor: ctor" << std::endl;
        throw std::runtime_error("some exception");
    }
    virtual ~WithThrowingCtor() { std::cout << "WithThrowingCtor: dctor" << std::endl; }
    void before() override { std::cout << "WithThrowingCtor: before" << std::endl; }
    void after() override { std::cout << "WithThrowingCtor: after" << std::endl; }
};
TEST_F(WithThrowingCtor, emptyTest) { std::cout << "WithThrowingCtor: emptyTest" << std::endl; }

class WithThrowingBefore : public rili::test::TestBaseFixture {
 public:
    WithThrowingBefore() : rili::test::TestBaseFixture() { std::cout << "WithThrowingBefore: ctor" << std::endl; }
    virtual ~WithThrowingBefore() { std::cout << "WithThrowingBefore: dctor" << std::endl; }
    void before() override {
        std::cout << "WithThrowingBefore: before" << std::endl;
        throw std::runtime_error("some exception");
    }
    void after() override { std::cout << "WithThrowingBefore: after" << std::endl; }
};
TEST_F(WithThrowingBefore, emptyTest) { std::cout << "WithThrowingBefore: emptyTest" << std::endl; }

class WithThrowingAfter : public rili::test::TestBaseFixture {
 public:
    WithThrowingAfter() : rili::test::TestBaseFixture() { std::cout << "WithThrowingAfter: ctor" << std::endl; }
    virtual ~WithThrowingAfter() { std::cout << "WithThrowingAfter: dctor" << std::endl; }
    void before() override { std::cout << "WithThrowingAfter: before" << std::endl; }
    void after() override {
        std::cout << "WithThrowingAfter: after" << std::endl;
        throw std::runtime_error("some exception");
    }
};
TEST_F(WithThrowingAfter, emptyTest) { std::cout << "WithThrowingAfter: emptyTest" << std::endl; }
#endif
