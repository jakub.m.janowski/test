#include <rili/Test.hpp>
#include <rili/test/Config.hpp>
#include <rili/test/EventHandler.hpp>
#include <string>

TEST(Asserts, Pass) {
    ASSERT_EQ(1, 1);
    ASSERT_FALSE(false);
    ASSERT_GE(3, 3);
    ASSERT_GE(3, 2);
    ASSERT_GT(3, 2);
    ASSERT_LE(2, 3);
    ASSERT_LE(3, 3);
    ASSERT_LT(2, 3);
    ASSERT_NE(3, 7);
    ASSERT_TRUE(true);

#ifdef RILI_TEST_WITH_EXCEPTIONS
    ASSERT_ANY_THROW([]() { throw 5; });
    ASSERT_THROW(std::string, []() { throw std::string("10"); });
#endif
}

#ifdef RILI_TEST_WITH_EXCEPTIONS
TEST(Asserts, eqFailure) {
    ASSERT_EQ(1, 5);
    std::terminate();
}

TEST(Asserts, falseFailure) {
    ASSERT_FALSE(true);
    std::terminate();
}

TEST(Asserts, geFailure) {
    ASSERT_GE(1, 2);
    std::terminate();
}

TEST(Asserts, gtFailure) {
    ASSERT_GT(1, 2);
    std::terminate();
}

TEST(Asserts, leFailure) {
    ASSERT_LE(2, 1);
    std::terminate();
}

TEST(Asserts, ltFailure) {
    ASSERT_LT(1, 1);
    std::terminate();
}

TEST(Asserts, neFailure) {
    ASSERT_NE(1, 1);
    std::terminate();
}

TEST(Asserts, trueFailure) {
    ASSERT_TRUE(false);
    std::terminate();
}

TEST(Asserts, anyThrowFailure) {
    ASSERT_ANY_THROW([]() {});
    std::terminate();
}

TEST(Asserts, throw1Failure) {
    ASSERT_THROW(int, []() { throw std::string(":)"); });
    std::terminate();
}

TEST(Asserts, throw2Failure) {
    ASSERT_THROW(int, []() {});
    std::terminate();
}
#endif
