#include <rili/Test.hpp>
#include <rili/test/Config.hpp>
#include <rili/test/EventHandler.hpp>
#include "Assert.hpp"
/*
 * EXAMPLES_ASSERTs are not needed when you use rtest expectations - however in rtest project we use examples as self
 * tests, so EXAMPLES_ASSERTs are here only because of that.
 */

class ExpectSuccess : public rili::test::TestBaseFixture {
 public:
    void after() override { EXAMPLES_ASSERT(!rili::test::EventHandler::getInstance().currentTestFailed()); }
};

class ExpectFailure : public rili::test::TestBaseFixture {
 public:
    void after() override { EXAMPLES_ASSERT(rili::test::EventHandler::getInstance().currentTestFailed()); }
};

TEST_F(ExpectFailure, AddFailure) {
    ADD_FAILURE("some message");
    ADD_FAILURE();
}

TEST_F(ExpectSuccess, EQ) {
    EXAMPLES_ASSERT(EXPECT_EQ(1, 1, "1 should be always equal to 1"));
    EXAMPLES_ASSERT(EXPECT_EQ(2, 2));
}

TEST_F(ExpectFailure, EQ) {
    EXAMPLES_ASSERT(!EXPECT_EQ(1, 2, "1 should never be equal to 2"));
    EXAMPLES_ASSERT(!EXPECT_EQ(2, 1));
}

TEST_F(ExpectSuccess, NE) {
    EXAMPLES_ASSERT(EXPECT_NE(1, 2, "1 should always be not equal to 2"));
    EXAMPLES_ASSERT(EXPECT_NE(2, 1));
}

TEST_F(ExpectFailure, NE) {
    EXAMPLES_ASSERT(!EXPECT_NE(1, 1, "1 should never be not equal to 1"));
    EXAMPLES_ASSERT(!EXPECT_NE(2, 2));
}

TEST_F(ExpectSuccess, LT) {
    EXAMPLES_ASSERT(EXPECT_LT(1, 2, "1 should be always less than to 2"));
    EXAMPLES_ASSERT(EXPECT_LT(2, 3));
}

TEST_F(ExpectFailure, LT) {
    EXAMPLES_ASSERT(!EXPECT_LT(2, 1, "2 should never be less than to 1"));
    EXAMPLES_ASSERT(!EXPECT_LT(3, 2));
}

TEST_F(ExpectSuccess, LE) {
    EXAMPLES_ASSERT(EXPECT_LE(1, 2, "1 should be always less or equal 2"));
    EXAMPLES_ASSERT(EXPECT_LE(2, 3));

    EXAMPLES_ASSERT(EXPECT_LE(1, 1, "1 should be always less or equal 1"));
    EXAMPLES_ASSERT(EXPECT_LE(2, 2));
}

TEST_F(ExpectFailure, LE) {
    EXAMPLES_ASSERT(!EXPECT_LE(2, 1, "2 should never be less or equal 1"));
    EXAMPLES_ASSERT(!EXPECT_LE(3, 2));
}

TEST_F(ExpectSuccess, GT) {
    EXAMPLES_ASSERT(EXPECT_GT(2, 1, "2 should be always greater than to 1"));
    EXAMPLES_ASSERT(EXPECT_GT(3, 2));
}

TEST_F(ExpectFailure, GT) {
    EXAMPLES_ASSERT(!EXPECT_GT(1, 2, "1 should never be greater than to 2"));
    EXAMPLES_ASSERT(!EXPECT_GT(2, 3));
}

TEST_F(ExpectSuccess, GE) {
    EXAMPLES_ASSERT(EXPECT_GE(2, 1, "2 should be always greater or equal 1"));
    EXAMPLES_ASSERT(EXPECT_GE(3, 2));

    EXAMPLES_ASSERT(EXPECT_GE(1, 1, "1 should be always greater or equal 1"));
    EXAMPLES_ASSERT(EXPECT_GE(2, 2));
}

TEST_F(ExpectFailure, GE) {
    EXAMPLES_ASSERT(!EXPECT_GE(1, 2, "1 should never be greater or equal 2"));
    EXAMPLES_ASSERT(!EXPECT_GE(2, 3));
}

TEST_F(ExpectSuccess, TRUE) {
    EXAMPLES_ASSERT(EXPECT_TRUE(true, "true should be always true"));
    EXAMPLES_ASSERT(EXPECT_TRUE(!false));
}

TEST_F(ExpectFailure, TRUE) {
    EXAMPLES_ASSERT(!EXPECT_TRUE(!true, "!true should never be true"));
    EXAMPLES_ASSERT(!EXPECT_TRUE(false));
}

TEST_F(ExpectSuccess, FALSE) {
    EXAMPLES_ASSERT(EXPECT_FALSE(false, "false should be always false"));
    EXAMPLES_ASSERT(EXPECT_FALSE(!true));
}

TEST_F(ExpectFailure, FALSE) {
    EXAMPLES_ASSERT(!EXPECT_FALSE(!false, "!false should never be false"));
    EXAMPLES_ASSERT(!EXPECT_FALSE(true));
}
#ifdef RILI_TEST_WITH_EXCEPTIONS
TEST_F(ExpectSuccess, ANY_THROW) {
    EXAMPLES_ASSERT(EXPECT_ANY_THROW([]() { throw 1; }, "throwing function should always throw"));
    EXAMPLES_ASSERT(EXPECT_ANY_THROW([]() { throw 2; }));
}

TEST_F(ExpectFailure, ANY_THROW) {
    EXAMPLES_ASSERT(!EXPECT_ANY_THROW([]() {}, "empty function should never throw"));
    EXAMPLES_ASSERT(!EXPECT_ANY_THROW([]() {}));
}

TEST_F(ExpectSuccess, THROW) {
    EXAMPLES_ASSERT(EXPECT_THROW(std::runtime_error, []() { throw std::runtime_error("expected"); },
                                 "throwing function should always throw"));
    EXAMPLES_ASSERT(EXPECT_THROW(std::exception, []() { throw std::runtime_error("some other"); }));
}

TEST_F(ExpectFailure, THROW) {
    EXAMPLES_ASSERT(!EXPECT_THROW(std::exception, []() {}, "empty function should never throw"));
    EXAMPLES_ASSERT(!EXPECT_THROW(std::exception, []() {}));

    EXAMPLES_ASSERT(
        !EXPECT_THROW(std::exception, []() { throw 1; }, "int throwing function should never throw std::excaption"));
    EXAMPLES_ASSERT(!EXPECT_THROW(std::exception, []() { throw 2; }));
}
#endif

TEST_F(ExpectSuccess, NumberOfTestGreaterThanZero) { EXPECT_GT(rili::test::TestStorage::getInstance().size(), 0u); }
