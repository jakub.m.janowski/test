#include <rili/test/Config.hpp>
#include <rili/test/InternalException.hxx>
#include <rili/test/MockDefinitions.hpp>

namespace rili {
namespace test {
namespace mock {
#ifdef RILI_TEST_WITH_EXCEPTIONS
[[noreturn]] void onUnexpectedCall() { throw detail::UnexpectedCall(); }
#else
[[noreturn]] void onUnexpectedCall() { std::terminate(); }
#endif
}  // namespace mock
}  // namespace test
}  // namespace rili
