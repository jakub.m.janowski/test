#pragma once
/** @file */

#include <rili/test/BaseFixture.hpp>
#include <rili/test/TestStorage.hpp>
#include <string>

/**
  * @brief Define and register single test in rili::test::TestStorage using provided fixture(CASE_NAME)
  *
  * override rili::TestBaseFixture::run() method.
  *
  * @note all test names need to be unique (CASE_NAME + SCENARIO_NAME)
  * @hideinitializer
  */
#define TEST_F(CASE_NAME, SCENARIO_NAME)                                                                              \
    class riliTest##CASE_NAME##SCENARIO_NAME : public CASE_NAME {                                                     \
     public:                                                                                                          \
        riliTest##CASE_NAME##SCENARIO_NAME() : CASE_NAME(), m_caseName(#CASE_NAME), m_scenarioName(#SCENARIO_NAME) {} \
        void run() override;                                                                                          \
        std::string const& fixtureName() const noexcept override { return m_caseName; }                               \
        std::string const& scenarioName() const noexcept override { return m_scenarioName; }                          \
        virtual ~riliTest##CASE_NAME##SCENARIO_NAME() = default;                                                      \
                                                                                                                      \
     private:                                                                                                         \
        static ::rili::test::TestCreator<riliTest##CASE_NAME##SCENARIO_NAME> m_creator;                               \
        const std::string m_caseName;                                                                                 \
        const std::string m_scenarioName;                                                                             \
    };                                                                                                                \
    ::rili::test::TestCreator<riliTest##CASE_NAME##SCENARIO_NAME> riliTest##CASE_NAME##SCENARIO_NAME::m_creator(      \
        #CASE_NAME, #SCENARIO_NAME);                                                                                  \
    void riliTest##CASE_NAME##SCENARIO_NAME::run()

/**
  * @brief Define and register single test in rili::test::TestStorage using rili::test::TestBaseFixture
  *
  * override rili::TestBaseFixture::run() method.
  *
  * @note all test names need to be unique (CASE_NAME + SCENARIO_NAME)
  * @hideinitializer
  */
#define TEST(CASE_NAME, SCENARIO_NAME)             \
    typedef rili::test::TestBaseFixture CASE_NAME; \
    TEST_F(CASE_NAME, SCENARIO_NAME)
