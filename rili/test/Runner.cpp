#include <rili/test/CallHandler.hpp>
#include <rili/test/Config.hpp>
#include <rili/test/EventHandler.hpp>
#include <rili/test/InternalException.hxx>
#include <rili/test/Runner.hpp>
#include <rili/test/TestStorage.hpp>
#include <string>

namespace rili {
namespace test {
namespace runner {
#ifdef RILI_TEST_WITH_EXCEPTIONS
namespace {
std::string convert(const std::exception_ptr& e) {
    try {
        std::rethrow_exception(e);
    } catch (std::exception& e) {
        return std::string("(std::exception): ") + e.what();
    } catch (...) {
        return "unknown exception";
    }
}
}  // namespace
#endif

bool runSingleTest(rili::test::TestCreatorBase const& creator) {
    auto& handler = EventHandler::getInstance();
    handler.startTest(creator.fixtureName(), creator.scenarioName());
#ifdef RILI_TEST_WITH_EXCEPTIONS
    try {
        auto test = creator.createTest();
        if (!handler.currentTestFailed()) {
            try {
                test->before();
                if (!handler.currentTestFailed()) {
                    try {
                        test->run();
                    } catch (detail::UnexpectedCall const&) {
                    } catch (detail::FailedAssertion const&) {
                    } catch (...) {
                        handler.runFailed(convert(std::current_exception()));
                    }
                    try {
                        test->after();
                    } catch (detail::UnexpectedCall const&) {
                    } catch (detail::FailedAssertion const&) {
                    } catch (...) {
                        handler.afterFailed(convert(std::current_exception()));
                    }
                }
            } catch (detail::UnexpectedCall const&) {
            } catch (detail::FailedAssertion const&) {
            } catch (...) {
                handler.beforeFailed(convert(std::current_exception()));
            }
        }
    } catch (detail::UnexpectedCall const&) {
    } catch (detail::FailedAssertion const&) {
    } catch (...) {
        handler.createFailed(convert(std::current_exception()));
    }
#else
    auto test = creator.createTest();
    if (!handler.currentTestFailed()) {
        test->before();
        if (!handler.currentTestFailed()) {
            test->run();
            test->after();
        }
    }
#endif
    CallHandler::getInstance().tearDown();
    return handler.endTest();
}

bool run(const std::function<FilteringResult(const std::string&, const std::string&)>& filter) {
    bool regressionResult = true;

    EventHandler::getInstance().startTests();
    auto const& storage = rili::test::TestStorage::getInstance();
    for (auto const& creator : storage) {
        switch (filter(creator.get().fixtureName(), creator.get().scenarioName())) {
            case FilteringResult::Run: {
                regressionResult = runSingleTest(creator.get()) ? regressionResult : false;
                break;
            }
            case FilteringResult::Disable: {
                EventHandler::getInstance().testDisabled(creator.get().fixtureName(), creator.get().scenarioName());
                break;
            }
            case FilteringResult::Skip: {
                EventHandler::getInstance().testSkipped(creator.get().fixtureName(), creator.get().scenarioName());
                break;
            }
        }
    }

    EventHandler::getInstance().endTests();
    return regressionResult;
}

}  // namespace runner
}  // namespace test
}  // namespace rili
