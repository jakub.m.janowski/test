#pragma once
/** @file */

#include <functional>
#include <rili/test/BaseFixture.hpp>
#include <string>

namespace rili {
namespace test {
namespace runner {

/**
 * @brief is enum which can be used to choose if test should run, be skipped or disabled
 */
enum class FilteringResult { Run, Skip, Disable };

/**
 * @brief is function used to filter tests.
 */
typedef std::function<FilteringResult(std::string const& fixtureName, std::string const& scenarioName)> Filter;
/**
 * @brief is used to run one time all tests.
 * @param filter is function which should return if test should be run normally, marked as disabled or skipped
 * @return regression result - true if pass. false in case of any failure
 */
bool run(Filter const& filter = [](std::string const&, std::string const& name) {
    if (name.substr(0, 9) == "DISABLED_") {
        return FilteringResult::Disable;
    } else {
        return FilteringResult::Run;
    }
});

/**
 * @brief run single test
 * @param creator is test creator
 * @return test result - true if pass. false in case of any failure
 */
bool runSingleTest(TestCreatorBase const& creator);

}  // namespace runner
}  // namespace test
}  // namespace rili
