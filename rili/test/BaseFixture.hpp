#pragma once
/** @file */

#include <memory>
#include <rili/MakeUnique.hpp>
#include <string>

namespace rili {
namespace test {
/**
 * @brief The TestBaseFixture class is base class for test fixture used directly by TEST macro
 * and indirect by TEST_F because TestBaseFixture must be base class for use provided fixture
 */
class TestBaseFixture {
 public:
    /**
     * @brief will be run as preconditional for test
     * @note you can override it in your fixture if you have special preconditions for your test
     */
    virtual void before();
    /**
     * @brief should contain test body - will be provided by TEST_F or TEST
     */
    virtual void run() = 0;
    /**
     * @brief will be run after test
     * @note you can use it to check test post conditions
     * @note you can use it to release resources aquired in before() or run() and preform teardowns
     */
    virtual void after();

 public:
    TestBaseFixture() = default;
    virtual ~TestBaseFixture() = default;
    /**
     * @brief used to get test fixture name
     *
     * **example:**
     * ```
     * TEST(XYZ, ABC)
     * {
     *     std::cout << fixtureName() << std::endl;
     * }
     * // will print "XYZ"
     * ```
     * @return fixture name as string
     */
    virtual std::string const& fixtureName() const noexcept = 0;
    /**
     * @brief used to get test scenario name
     *
     * **example:**
     * ```
     * TEST(XYZ, ABC)
     * {
     *     std::cout << scenarioName() << std::endl;
     * }
     * // will print "ABC"
     * ```
     * @return scenario name as string
     */
    virtual std::string const& scenarioName() const noexcept = 0;

 private:
    TestBaseFixture(TestBaseFixture const& other) = delete;
    TestBaseFixture& operator=(TestBaseFixture const&) = delete;
};

/// @cond INTERNAL
class TestCreatorBase {
 public:
    typedef std::unique_ptr<TestBaseFixture> TestPtr;
    virtual TestPtr createTest() const = 0;
    virtual ~TestCreatorBase() = default;
    std::string const& fixtureName() const;
    std::string const& scenarioName() const;

 protected:
    TestCreatorBase(std::string const& fixtureName, std::string const& scenarioName);

 private:
    TestCreatorBase(TestCreatorBase const& other) = delete;
    TestCreatorBase& operator=(TestCreatorBase const&) = delete;

 private:
    const std::string m_fixtureName;
    const std::string m_scenarioName;
};

template <typename TestType>
class TestCreator final : public TestCreatorBase {
 public:
    TestPtr createTest() const override { return rili::make_unique<TestType>(); }
    TestCreator(std::string const& fixtureName, std::string const& scenarioName)
        : TestCreatorBase(fixtureName, scenarioName) {}
};
/// @endcond INTERNAL
}  // namespace test
}  // namespace rili
