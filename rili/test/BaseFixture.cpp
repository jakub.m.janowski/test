#include <rili/test/BaseFixture.hpp>
#include <rili/test/TestStorage.hpp>
#include <string>

namespace rili {
namespace test {
void TestBaseFixture::before() {}

void TestBaseFixture::after() {}

const std::string& TestCreatorBase::fixtureName() const { return m_fixtureName; }

const std::string& TestCreatorBase::scenarioName() const { return m_scenarioName; }

TestCreatorBase::TestCreatorBase(const std::string& fixtureName, const std::string& scenarioName)
    : m_fixtureName(fixtureName), m_scenarioName(scenarioName) {
    TestStorage::getInstance().registerTestCreator(*this);
}
}  // namespace test
}  // namespace rili
